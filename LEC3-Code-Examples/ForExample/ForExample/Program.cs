﻿using System;

namespace ForExample
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Write code to:
             * (a)
             * Prompt the user for a number
             * Loop that number of times
             * Print out the number of each loop
             */

            //Console.Write("Enter num 0-20: ");
            //int iNum = Convert.ToInt32(Console.ReadLine());

            //// Loop iNum times
            //for (int i = 1; i <= iNum; i++)
            //{
            //    Console.WriteLine($"Loop: {i}");
            //}

            /* (b)
             * Extend to show nested loop, continue and break
             */
            Console.Write("Enter num 0-20: ");
            int iNum = Convert.ToInt32(Console.ReadLine());

            int stopNum = 5;

            // Outer Loop print loop count: iNum times
            for (int i = 1; i <= iNum; i++)
            {
                // BREAK if stopNum reached
                if (i > stopNum)
                {
                    Console.WriteLine("Limit reached");
                    break;
                }
                Console.Write($"Loop {i}: ");

                // NESTED LOOP
                for (int j = 1; j <= 10; j++)
                {
                    // CONTINUE if not even loop number
                    if (j % 2 != 0)
                    {
                        continue;
                    }
                    Console.Write($"{j} ");
                }
                Console.WriteLine();
            }
        }
    }
}
