﻿using System;

namespace FunctionExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please select A B or C: ");
            var iStr = Console.ReadLine().ToUpper();
            
            if (iStr.Equals("A"))
            {
                myFunctionA();
            }
            else if (iStr.Equals("B"))
            {
                var bStr = myFunctionB();
                Console.WriteLine($"{bStr}");
            }
            else if (iStr.Equals("C"))
            {
                var rInt = myFunctionC(10);
                Console.WriteLine($"Returned {rInt}");
            }
            else
                Console.WriteLine("Invalid Input");

        }
        // no parameters no return value
        static void myFunctionA()
        {
            Console.WriteLine("valid");
        }
        // return value
        static string myFunctionB()
        {
            var rString = "B String";
            return rString;
        }
        // return value + parameter
        static int myFunctionC(int iIn)
        {
            Console.WriteLine($"Input {iIn}");
           return ++iIn;
        }
    }
}
