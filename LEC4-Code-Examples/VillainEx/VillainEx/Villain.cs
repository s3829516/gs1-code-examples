﻿using System;
namespace VillainEx
{
    [Serializable]
    public class Villain
    {

        // FIELDS
        private string _name;
        private string _weapon;

        // CONSTRUCTORS
        public Villain()
        {
            _name = "unknown";
            _weapon = "unknown";
        }
        public Villain(string name, string weapon)
        {
            _name = name;
            _weapon = weapon;
        }

        //PROPERTIES
        //public string Name
        //{
        //    get => _name;
        //    set => _name = value;
        //}
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string Weapon
        {
            get => _weapon;
            set => _weapon = value;
        }

        // METHODS
        public void Print()
        {
            Console.WriteLine($"{_name} armed with {Weapon}");
        }
    }
}
