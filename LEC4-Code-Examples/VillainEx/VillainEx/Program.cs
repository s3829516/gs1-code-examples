﻿using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace VillainEx
{
    class Program
    {
        static void Main(string[] args)
        {
            // CREATE OBJECT
            Villain villain1 = new Villain();
            //Villain villain2 = new Villain("StormTrooper", "Blaster");

            Console.WriteLine($"villain1 initially: {villain1.Name} armed with {villain1.Weapon}");
            //Console.WriteLine($"Villain2: {villain2.Name} armed with {villain2.Weapon}");

            villain1.Name = "DarthVadar";
            villain1.Weapon = "TheForce";
            Console.Write("villain1 after setting values: ");
            villain1.Print();

            //// SERIALIZATION EXAMPLE
            //// (1) get the path and filename
            string path = Directory.GetCurrentDirectory();
            var dirSeparator = Path.DirectorySeparatorChar;
            path += dirSeparator + "testfile";

            Console.WriteLine($"Path: {path}");

            //// (2) formatter
            //IFormatter formatter = new BinaryFormatter();
            //// (3)  stream to write to
            //Stream stream = new FileStream(path, FileMode.Create);
            //// (4) serialize object
            //formatter.Serialize(stream, villain1);
            //stream.Close();

            // Deserialise Example
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(path, FileMode.Open);

            Villain villainDS = new Villain();
            Console.Write("villainDS initially: ");
            villainDS.Print();
            villainDS = (Villain)formatter.Deserialize(stream);
            Console.Write("villainDS after deserialize: ");
            villainDS.Print();
        }
    }
}
