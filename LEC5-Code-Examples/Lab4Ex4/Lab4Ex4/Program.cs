﻿using System;

namespace Lab4Ex4
{
    class Program
    {
        static void Main(string[] args)
        {
            GameState game = new GameState();

            // Example: testing if moving north is valid (is cell in board?)
            int playerIndex = game.playerPos();
            Console.WriteLine($"Player Index: {playerIndex}");

            // Example: is Move NORTH VALID?
            int northIndex = game.northIndex();
            if (northIndex != -1)
            {
                Console.WriteLine($"Valid move to {northIndex}");
            }
            else
                Console.WriteLine($"Invalid Move NORTH");
        }
    }
}
