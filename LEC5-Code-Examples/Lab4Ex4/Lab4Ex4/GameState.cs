﻿using System;
namespace Lab4Ex4
{
    public class GameState
    {
        // Example: declare array
        // In your game these elements will be randomly placed
        private char[] myGame =
            { '_', 'T', '_', '_', 'P', '_', '_', '_', '_', 'H' };
        private int width = 3;
        private int height = 3;

        public GameState()
        {
        }

        // Example method
        public int playerPos()
        {
            //return Array.IndexOf(gameBoard, 'P');
            for (int i = 0; i < myGame.Length; i++)
            {
                if (myGame[i].Equals('P'))
                    return i;
            }
            return -1;
        }
        public int northIndex()
        {
            int rInt = -1;
            int nPos = playerPos() + width;
            if (nPos <  width * height)
            {
                rInt = nPos;
            }
            return rInt;
        }
    }
}
