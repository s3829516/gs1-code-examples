﻿using System;
namespace GameSkeleton
{
    public class GameState
    {
        // Fields
        // ======
        private static char[] _theBoard;
        // What size game board?
        // How many hazards and treasure
        private int _xSize; // = ??;
        private int _ySize; // = ??;
        private int numHazards; // = ??;
        private int numTreasure; //  = ??;

        // what can the player see
        private bool _seeH = false;
        private bool _seeT = false;
        private bool _seeX = false;

        // is the player atExit?
        private bool _atExit = false;

        // how much treasure has the player collected?
        private int _treasure = 0;

        // what is the exit square? (useful if the player moves away from the exit square)
        private int _theExit = -1;


        // Constructor(s)
        // ==============
        public GameState()
        {
            // initialise the array representing the gameboard (see lab5 exercise1)
            // initialise all cells to empty

            // place the player (see: lab4 exercise3)
            // place the treasure positions
            // place the exit
            // place the hazard positions
        }

        // Properties
        // ==========
        public int Treasure
        {
            // your code goes here: get and set?
        }
        public bool AtExit
        {
            // your code goes here: just get?
        }


        // Methods
        // =======
        private void placePlayer()
        {
            // your code goes here - place player in random position on board
        }
        private void placeExit()
        {
            // your code goes here - place exit in random position on board
        }
        private void placeTreasure()
        {
            // your code goes here - place N items of treasure in random position(s) on board
        }
        private void placeHazard()
        {
            // your code goes here - place N hazards in random position(s) on board
        }


        public bool isClear(int index)
        {
            // check if index position is clear
        }

        public bool canMove(int index)
        {
            // check if player can move to the index position (if it is clear, has treasure or exit)
        }

        public bool hazardMove(int index)
        {
            // check if the index position is a hazard square
        }

        public void movePlayer(int to)
        {
            // move the player from their current position to index position "to"
        }

        public int playerPos()
        {
            // your code goes here - return the index position of the player (see: lab4 ex4)
        }

        public int cellSouth()
        {
            // return index position of cell to south (or -1 if invalid)
        }
        public int cellNorth()
        {
            // return index position of cell to north (or -1 if invalid)
        }
        public int cellEast()
        {
            // return index position of cell to east (or -1 if invalid)
        }
        public int cellWest()
        {
            // return index position of cell to west (or -1 if invalid)
        }

        public void checkTreasure(int cell)
        {
            // check if cell is treasure - collect it
        }
        public void checkExit(int cell)
        {
            // check if cell is exit - take appropriate action
        }

        public void setPercepts()
        {
            // your code goes here (see: lab4 exercise 5)
            //  check for and set values for Hazard, Treasure, Exit in adjacent cells
        }

        private int rdmClearCell()
        {
            // return randomly generate index position in board that is clear/empty (see: lab4 ex3)
        }

        public void printPercepts()
        {
            // for any Hazard, Treasure, Exit that player is adjacent to - display message
        }

        public void printBoard()
        {
            // your code goes here 
            //      - either display array (for debugging) or pretty print as 2D cheat map
        }
    }
}

