﻿using System;

namespace BooleanExample
{
    class Program
    {
        static void Main(string[] args)
        {
            // declaring boolean values
            bool bVar1 = new Boolean();
            bool bVar2 = true;
            Console.WriteLine($"bVar1 is {bVar1}, bVar2 = {bVar2}");

            // assigning value to bool variable
            bVar1 = true;
            Console.WriteLine($"bVar is {bVar1}");

            // evaluating bool as test condition
            int i1 = 10;
            int i2 = 20;
            if (i1 < i2)
                Console.WriteLine("Smaller");

            if (bVar1)
                Console.WriteLine("bVar1 is true");
        }
    }
}
