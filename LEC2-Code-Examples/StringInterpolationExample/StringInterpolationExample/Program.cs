﻿using System;

namespace StringInterpolationExample
{
    class Program
    {
        static void Main(string[] args)
        {
            // Example of String formatting
            string str1 = "left";
            string str2 = "right";
            Console.WriteLine($"|{str1,-10}|{str2,10}|");
            Console.WriteLine();

            // Example with real numbers
            float fVar1 = 123.456f;
            double dVar1 = 987.654321;
            Console.WriteLine($"fVar1 is: |{fVar1,10}|");
            Console.WriteLine($"dVar1 is: |{dVar1,10}|");
            // dVar1 with precision specified: 2 decimal places
            Console.WriteLine($"dVar1 is: |{dVar1,10:N2}|");
        }
    }
}
