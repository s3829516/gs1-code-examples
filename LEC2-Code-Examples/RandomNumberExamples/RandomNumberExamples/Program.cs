﻿using System;

namespace RandomNumberExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            // Random Number Examples
            // Random rdm = new Random();
            var rand = new Random();

            // Next: overloaded method
            // (1) int Random.Next() returns a non-negative random integer
            var rdm1 = rand.Next();
            // (2) int Random.Next(int MaxValue) returns a non-negative
            //      random integer that is less than MaxValue (exclusive)
            var rdm2 = rand.Next(1000);
            // (2) int Random.Next(int MinValue, int MaxValue) returns a non-negative
            //      random integer within the range
            //      MinValue is inclusive, MaxValue is exclusive 
            var rdm3 = rand.Next(20, 50);

            Console.WriteLine($"rdm1: {rdm1}, rdm2: {rdm2}, rdm3: {rdm3}");
        }
    }
}
