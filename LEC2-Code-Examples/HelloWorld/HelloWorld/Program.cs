﻿using System;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            //without using directive
            //System.Console.WriteLine("Hello World!");

            Console.WriteLine("Hello World!");

            //Reading and writing input from the console
            Console.Write("Hello what is your name? ");
            string uStr = Console.ReadLine();
            Console.WriteLine($"Hello {uStr}");
        }
    }
}
