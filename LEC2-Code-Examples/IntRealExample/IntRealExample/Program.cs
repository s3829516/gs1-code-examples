﻿using System;

namespace IntRealExample
{
    class Program
    {
        static void Main(string[] args)
        {
            // initialise float
            float fVar1 = 123.456F;

            // initialise double
            double dVar1 = 123.456;

            // initialise decimal
            decimal mVar1 = 123.456M;

            Console.WriteLine($"fVar1 is {fVar1}");


            //initialise float with int
            int iVar1 = 123;
            float fVar2 = 456.87;

            iVar1 = (int) fVar2;





        }
    }
}
